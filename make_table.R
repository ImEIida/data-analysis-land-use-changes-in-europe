make_table <- function(dists, lt_id, reg_id, folder_path) {
  fid = paste(reg_id, lt_id, sep="")
  fname = paste(folder_path, fid, ".tbl", sep="")
  
 # cat("\\begin{table} \n", file=fname, append=TRUE)
 # cat("\\small \n", file=fname, append=TRUE)
 # cat("\\caption*{Distance measures with respect to the \\textsc{t}eruti\\textsc{o}ld} \n", file=fname, append=TRUE)
 # cat("\\hfill{} \n", file=fname, append=TRUE)
  cat("\\begin{tabular}{ | l | c | c | c | c |} \n", file=fname, append=TRUE)
  cat("\\hline \n", file=fname, append=TRUE)
  cat("\\multirow{2}{*}{\\textsc{datasets}} & \\multicolumn{4}{c|}{\\textsc{distance measures}} \\\\ \n", file=fname, append=TRUE)
  cat("& \\textbf{dtw} & \\textbf{pc} & \\textbf{dd} & \\textbf{sts} \\\\ \\hline \n", file=fname, append=TRUE)
  cat("\\hline \n", file=fname, append=TRUE)
  
  db_sources <- ls(dists)
  mat <- new.env()
  for (i in 1:length(db_sources)) {
    db = db_sources[i]
    dist_entry = dists[[db]]
    
    #cat(db, file=fname, append=TRUE)

    skip_row <- TRUE
    for (j in 1:length(dist_entry)) {
      if (!is.na(dist_entry[j])) {
        skip_row <- FALSE
        break
      }
    }
    
    if (!skip_row) {
      cat(gsub("_", "\\_", db, fixed=TRUE), file=fname, append=TRUE)
      
      for (j in 1:length(dist_entry)) {
        if (!is.na(dist_entry[j])) {
          cat(paste(" & ", dist_entry[j], sep=""), file=fname, append=TRUE)
        } else {
          cat(" & -", file=fname, append=TRUE)
        }
      }
      cat("\\\\ \\hline \n", file=fname, append=TRUE)      
    }
  }
  
  cat("\\end{tabular} \n", file=fname, append=TRUE)
 # cat("\\hfill{} \n", file=fname, append=TRUE)
 # cat("\\caption{TODO} \n", file=fname, append=TRUE)
 # cat(paste("\\label{tbl:", fid, "} \n", sep=""), file=fname, append=TRUE)
  #cat("\\end{table} \n", file=fname, append=TRUE)
  
}